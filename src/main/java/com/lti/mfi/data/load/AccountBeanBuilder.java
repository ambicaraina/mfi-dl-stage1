package com.lti.mfi.data.load;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.lti.ivaps.mfi.beans.Account;
import com.lti.ivaps.mfi.beans.Address;
import com.lti.ivaps.mfi.beans.MfiDomain;
import com.lti.ivaps.mfi.constans.MfiConstants;
import com.lti.ivaps.mfi.validation.AccountValidation;
import com.lti.ivaps.mfi.validation.AddressValidation;

public class AccountBeanBuilder {
	public static List<Account> buidAccountBean(String record, MfiDomain mfiDomain){
/*		String[] split = record.split("&");
		Account account = new Account();
		for (String s : split) {
			if (s.contains("ACTCRD")) {
				String[] splitPreserveAllTokens = StringUtils.splitPreserveAllTokens(s, "~");
				
				 * for(String token : splitPreserveAllTokens){
				 * System.out.println(token); }
				 

				account.setSegmentIdentifier(splitPreserveAllTokens[0]);
				account.setUniqueAccRefNumber(splitPreserveAllTokens[1]);
				account.setAccNumber(splitPreserveAllTokens[2]);
				account.setBranchIdentifier(splitPreserveAllTokens[3]);
				account.setKendraCentreIdentifier(splitPreserveAllTokens[4]);
				account.setLoanOfficerForOriginatingTheLoan(splitPreserveAllTokens[0]);
				account.setDateOfAccInfo(splitPreserveAllTokens[1]);
				account.setLoanCategory(splitPreserveAllTokens[2]);
				account.setGroupIdentifier(splitPreserveAllTokens[6]);
				account.setLoanCycleId(splitPreserveAllTokens[7]);
				account.setLoanPurpose(splitPreserveAllTokens[8]);
				account.setAccStatus(splitPreserveAllTokens[9]);
				account.setApplicationDate(splitPreserveAllTokens[10]);
				account.setSanctionedDate(splitPreserveAllTokens[11]);
				account.setDateOpened(splitPreserveAllTokens[12]);
				account.setDateClosed(splitPreserveAllTokens[13]);
				account.setAppliedForAmount(splitPreserveAllTokens[14]);
				account.setLoanAmountSanctioned(splitPreserveAllTokens[15]);
				account.setTotalAmountDisbursed(splitPreserveAllTokens[16]);
				account.setNumberOfInstallments(splitPreserveAllTokens[17]);
				account.setRepaymentFrequency(splitPreserveAllTokens[18]);
				account.setInstalmentAmount(splitPreserveAllTokens[19]);
				account.setCurrentBalance(splitPreserveAllTokens[20]);
				account.setAmountOverdue(splitPreserveAllTokens[21]);
				account.setDpd(splitPreserveAllTokens[22]);
				account.setWriteOffAmount(splitPreserveAllTokens[23]);
				account.setDateWriteOff(splitPreserveAllTokens[24]);
				account.setWriteOffReason(splitPreserveAllTokens[25]);
				account.setNoOfMeetingsHeld(splitPreserveAllTokens[26]);
				account.setNoOfMeetingsSkipped(splitPreserveAllTokens[27]);
				account.setInsuranceIndicator(splitPreserveAllTokens[28]);
				account.setTypeOfInsurance(splitPreserveAllTokens[29]);
				account.setSumAssured(splitPreserveAllTokens[30]);
				account.setAgreedMeetingDayOfTheWeek(splitPreserveAllTokens[31]);
				account.setAgreedMeetingTimeOfTheDay(splitPreserveAllTokens[32]);
			}
		}

		return account;*/
		
		
		String[] splitPreserveAllTokens2 = StringUtils.splitPreserveAllTokens(record, MfiConstants.splitDelimiterDollar);
		List<Account> listAccount = new ArrayList<Account>();
		for(String s : splitPreserveAllTokens2){
			//System.out.println(s);
			if(s.contains(MfiConstants.ACTCRD)){
				Account account = new Account();
				String[] splitPreserveAllTokens = StringUtils.splitPreserveAllTokens(s, "~");
				/*for(String token : splitPreserveAllTokens){
					System.out.println(token);
				}*/

				account.setSegmentIdentifier(splitPreserveAllTokens[0]);
				account.setUniqueAccRefNumber(splitPreserveAllTokens[1]);
				account.setAccNumber(splitPreserveAllTokens[2]);
				account.setBranchIdentifier(splitPreserveAllTokens[3]);
				account.setKendraCentreIdentifier(splitPreserveAllTokens[4]);
				account.setLoanOfficerForOriginatingTheLoan(splitPreserveAllTokens[5]);
				account.setDateOfAccInfo(splitPreserveAllTokens[6]);
				account.setLoanCategory(splitPreserveAllTokens[7]);
				account.setGroupIdentifier(splitPreserveAllTokens[8]);
				account.setLoanCycleId(splitPreserveAllTokens[9]);
				account.setLoanPurpose(splitPreserveAllTokens[10]);
				account.setAccStatus(splitPreserveAllTokens[11]);
				account.setApplicationDate(splitPreserveAllTokens[12]);
				account.setSanctionedDate(splitPreserveAllTokens[13]);
				account.setDateOpened(splitPreserveAllTokens[14]);
				account.setDateClosed(splitPreserveAllTokens[15]);
				account.setAppliedForAmount(splitPreserveAllTokens[16]);
				account.setLoanAmountSanctioned(splitPreserveAllTokens[17]);
				account.setTotalAmountDisbursed(splitPreserveAllTokens[18]);
				account.setNumberOfInstallments(splitPreserveAllTokens[19]);
				account.setRepaymentFrequency(splitPreserveAllTokens[20]);
				account.setInstalmentAmount(splitPreserveAllTokens[21]);
				account.setCurrentBalance(splitPreserveAllTokens[22]);
				account.setAmountOverdue(splitPreserveAllTokens[23]);
				account.setDpd(splitPreserveAllTokens[24]);
				account.setWriteOffAmount(splitPreserveAllTokens[25]);
				account.setDateWriteOff(splitPreserveAllTokens[26]);
				account.setWriteOffReason(splitPreserveAllTokens[27]);
				account.setNoOfMeetingsHeld(splitPreserveAllTokens[28]);
				account.setNoOfMeetingsSkipped(splitPreserveAllTokens[29]);
				account.setInsuranceIndicator(splitPreserveAllTokens[30]);
				account.setTypeOfInsurance(splitPreserveAllTokens[31]);
				account.setSumAssured(splitPreserveAllTokens[32]);
				account.setAgreedMeetingDayOfTheWeek(splitPreserveAllTokens[33]);
				account.setAgreedMeetingTimeOfTheDay(splitPreserveAllTokens[34]);
				
				//AccountValidation+++++++
				//AccountValidation.validateAccount(account, mfiDomain);
				
				if(null == AccountValidation.validateAccount(account, mfiDomain)){
					listAccount.add(account);
				}

				//listAccount.add(account);
			}
		}
		
		return listAccount;
	}
	
	public static void main(String[] args) {
		//MfiDomain mfiDomain = new MfiDomain();
		//List<Account> buidAccountBean = buidAccountBean("ACTCRD~NAYAGARH0036552~NAYAGARH0036552~NAYAGARH~0240019~MADAN MOHAN JENA MADAN MOHAN JENA~31082012~T02~0240000006720~1~A07~S07~18092012~22092012~25092012~05102013~05102013~12000~12000~12000~12~F03~0~0~0~0~~~~~~Y~L01~36000~~~~$ACTCRD~NAYAGARH0036552~NAYAGARH0036552~NAYAGARH~0240019~MADAN MOHAN JENA MADAN MOHAN JENA~31082012~T02~0240000006720~1~A07~S07~18092012~22092012~25092012~05102013~05102013~12000~12000~12000~12~F03~0~0~0~0~~~~~~Y~L01~36000~~~~$ACTCRD~NAYAGARH0036552~NAYAGARH0036552~NAYAGARH~0240019~MADAN MOHAN JENA MADAN MOHAN JENA~31082012~T02~0240000006720~1~A07~S07~18092012~22092012~25092012~05102013~05102013~12000~12000~12000~12~F03~0~0~0~0~~~~~~Y~L01~36000~~~~$ACTCRD~NAYAGARH0036552~NAYAGARH0036552~NAYAGARH~0240019~MADAN MOHAN JENA MADAN MOHAN JENA~31082012~T02~0240000006720~1~A07~S07~18092012~22092012~25092012~05102013~05102013~12000~12000~12000~12~F03~0~0~0~0~~~~~~Y~L01~36000~~~~", mfiDomain);
		//List<Account> buidAccountBean = buidAccountBean("ACTCRD~1101791000007321~1101791000007321~1101~11010001~RHRM~18112013~T03~1101000106~1~12~S04~23122011~23122011~23122011~~31082013~30000~30000~30000~24~F03~1498~5766~2996~51~~~X09~~~Y~L01~~WED~11:00~~", mfiDomain);
		/*for(Account a :buidAccountBean){
			System.out.println(a);
		}*/
	}

}
