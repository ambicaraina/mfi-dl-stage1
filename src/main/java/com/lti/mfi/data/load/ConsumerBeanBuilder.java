package com.lti.mfi.data.load;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.lti.ivaps.mfi.beans.Account;
import com.lti.ivaps.mfi.beans.Consumer;
import com.lti.ivaps.mfi.constans.MfiConstants;
import com.lti.ivaps.mfi.exceptions.CdfParsingException;

public class ConsumerBeanBuilder {
	
	public static List<Consumer> buildConsumer(String[] consumerSegment) {
		/*String[] consumerSegment2 = StringUtils.consumerSegment(record, MfiConstants.splitDelimiterDollar);
		List<Consumer> listConsumer = new ArrayList<Consumer>();
		for(String s : consumerSegment2){
			System.out.println("CNSCRD : " + s);
			if(s.contains(MfiConstants.CNSCRD)){
				//System.out.println("Here");
				Consumer consumer = new Consumer();
				String[] consumerSegment = StringUtils.consumerSegment(s, "~");
				for(String token : consumerSegment){
					System.out.println(token);
				}*/
				Consumer consumer = new Consumer();
				List<Consumer> listConsumer = new ArrayList<Consumer>();
				consumer.setSegmentIdentifier(consumerSegment[0]);
				consumer.setMemberIdentifier(consumerSegment[1]);
				consumer.setBranchIdentifier(consumerSegment[2]);
				consumer.setCentreIdentifier(consumerSegment[3]);
				consumer.setGroupIdentifier(consumerSegment[4]);
				//String[] consumerSegment1 = StringUtils.consumerSegment(consumerSegment[5], " ");

				consumer.setMemberFirstName((!consumerSegment[0].isEmpty()) ? consumerSegment[5] : "");
				consumer.setMemberMiddleName((!consumerSegment[1].isEmpty()) ? consumerSegment[6] : "");
				//if(consumerSegment1.length > 2){
					consumer.setMemberLastName((!consumerSegment[2].isEmpty()) ? consumerSegment[7] : "");
				//} else
				//{
					//consumer.setMemberLastName("");
				//}
				consumer.setMemberAltName(consumerSegment[8]);
				consumer.setMemberDOB(consumerSegment[9]);
				consumer.setMemberAge(consumerSegment[10]);
				consumer.setMemberAgeAsOnDate(consumerSegment[11]);
				consumer.setMemberGender(consumerSegment[12]);
				consumer.setMaritalStatus(consumerSegment[13]);
				consumer.setKeyPersonName(consumerSegment[14]);
				consumer.setKeyPersonRelnship(consumerSegment[15]);
				consumer.setRelativeNameOne(consumerSegment[16]);
				consumer.setRelativeRelnshipOne(consumerSegment[17]);
				consumer.setRelativeNameTwo(consumerSegment[18]);
				consumer.setRelativeRelnshipTwo(consumerSegment[19]);
				consumer.setRelativeNameThree(consumerSegment[20]);
				consumer.setRelativeRelnshipThree(consumerSegment[21]);
				consumer.setRelativeNameFour(consumerSegment[22]);
				consumer.setRelativeRelnshipFour(consumerSegment[23]);
				consumer.setNomineeName(consumerSegment[24]);
				consumer.setNomineeRelnship(consumerSegment[25]);
				consumer.setNomineeAge(consumerSegment[26]);
				consumer.setVoterId(consumerSegment[27]);
				consumer.setUid(consumerSegment[28]);
				consumer.setPan(consumerSegment[29]);
				consumer.setRationCard(consumerSegment[30]);
				consumer.setOtrIdTypeOneDesc(consumerSegment[31]);
				consumer.setOtrIdTypeOneValue(consumerSegment[32]);
				consumer.setOtrIdTypeTwoDesc(consumerSegment[33]);
				consumer.setOtherIdTypeTwoValue(consumerSegment[34]);
				consumer.setOtrIdTypeThreeDesc(consumerSegment[35]);
				consumer.setOtherIdTypeThreeValue(consumerSegment[36]);
				consumer.setTelephNumberTypeOneInd(consumerSegment[37]);
				consumer.setTelephNumberTypeOneValue(consumerSegment[38]);
				consumer.setTelephNumberTypeTwoInd(consumerSegment[39]);
				consumer.setTelephNumberTypeTwoValue(consumerSegment[40]);
				consumer.setPovertyIndex(consumerSegment[41]);
				consumer.setAssetOwnershipInd(consumerSegment[42]);
				consumer.setNoOfDependents(consumerSegment[43]);
				consumer.setBankName(consumerSegment[44]);
				consumer.setBranchName(consumerSegment[45]);
				consumer.setAccountNumber(consumerSegment[46]);
				consumer.setOccupation(consumerSegment[47]);
				consumer.setTotalMonthlyIncome(consumerSegment[48]);
				consumer.setTotalMonthlyExpenses(consumerSegment[49]);
				consumer.setMemberCaste(consumerSegment[50]);
				consumer.setMemberReligion(consumerSegment[51]);
				consumer.setGroupLeaderIndicator(consumerSegment[52]);
				consumer.setCenterLeaderIndicator(consumerSegment[53]);

				listConsumer.add(consumer);
			
		

		return listConsumer;
		
	}
	public static void main(String[] args) {
		System.out.println("START");
	//	List<Consumer> buildConsumer = buildConsumer("CNSCRD~024023453~NAYAGARH~0240116~024~KAILASHCHANDRA LENKA~~~~01011962~50~51~M~M01~DAMAYANTI LENAK~K02~~~~~~~~~DAMAYANTI LENAK~K06~~~~~~~~~~~~~~~~~~~~~~Z02~0~0~~O.B.C~~~~$CNSCRD~024023453~NAYAGARH~0240116~024~KAILASHCHANDRA LENKA~~~~01011962~50~51~M~M01~DAMAYANTI LENAK~K02~~~~~~~~~DAMAYANTI LENAK~K06~~~~~~~~~~~~~~~~~~~~~~Z02~0~0~~O.B.C~~~~$CNSCRD~024023453~NAYAGARH~0240116~024~KAILASHCHANDRA LENKA~~~~01011962~50~51~M~M01~DAMAYANTI LENAK~K02~~~~~~~~~DAMAYANTI LENAK~K06~~~~~~~~~~~~~~~~~~~~~~Z02~0~0~~O.B.C~~~~");
	//	for(Consumer c : buildConsumer){
		//	System.out.println(c);
		}
}

