package com.lti.ivaps.mfi.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.hive.ql.parse.HiveParser_IdentifiersParser.identifier_return;

import com.lti.ivaps.mfi.ExperianCRC32;
import com.lti.ivaps.mfi.exceptions.CdfValidationException;

public class MfiDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	String mfiId_;
	String memberId_;
	String rptDate_;

	String originalRecord_;

	String[] consumerSegment_;
	List<String[]> addressSegment_;
	List<String[]> accountSegment_;

	Consumer consumer_;
	List<Account> accountList_;
	List<Address> addressList_;

	Boolean valid_ = true;
	String reasonForFailure_ = null;

	Long globalCrc_;
	
	Boolean headerTrailerIndicator = false;
	
	String reasonOfFailureForCounter_ = null;
	
/*	public void addErrorCode(String errorCode) {
		if (StringUtils.isBlank(reasonForFailure_)) {
			reasonForFailure_ = errorCode;
		} else {
			if(!reasonForFailure_.contains(errorCode)){
			reasonForFailure_ += "," + errorCode;
			}
		}
	}*/
	
	public void addErrorCode(String errorCode) {
		if (StringUtils.isBlank(reasonForFailure_)) {
			reasonForFailure_ = errorCode;
		} else {
			if(!reasonForFailure_.contains(errorCode)){
			reasonForFailure_ += "," + errorCode;
			}
		}
		
		reasonOfFailureForCounter_ = reasonForFailure_;
	}
	
	
	//Ambica
	/*public void addErrorCode(String errorCode) {
		if (StringUtils.isBlank(reasonForFailure_)) {
			reasonForFailure_ = errorCode;
		} else {
			if(null == reasonOfFailureForCounter_){
				reasonOfFailureForCounter_ = errorCode;
			}else{
			reasonOfFailureForCounter_ += "," + errorCode;
			}
			if(!reasonForFailure_.contains(errorCode)){
			reasonForFailure_ += "," + errorCode;
			}
		}
	}*/

	public MfiDomain(String mfiId, String rptDate, String originalRecord) {
		super();
		//System.out.println("Inside constructor: " +mfiId +"Date: "+rptDate + "Original record: " +originalRecord);
		mfiId_ = mfiId;
		rptDate_ = rptDate;
		originalRecord_ = originalRecord;
	}

	public String getMfiId() {
		return mfiId_;
	}

	public void setMfiId(String mfiId) {
		mfiId_ = mfiId;
	}

	public String getMemberId() {
		return memberId_;
	}

	public void setMemberId(String memberId) {
		memberId_ = memberId;
	}

	public String getRptDate() {
		return rptDate_;
	}

	public void setRptDate(String rptDate) {
		rptDate_ = rptDate;
	}

	public String getOriginalRecord() {
		return originalRecord_;
	}

	public void setOriginalRecord(String originalRecord) {
		originalRecord_ = originalRecord;
	}

	public String[] getConsumerSegment() {
		return consumerSegment_;
	}

	public void setConsumerSegment(String[] consumerSegment) {
		consumerSegment_ = consumerSegment;
		
		consumer_ = BeanBuilderUtility.buildConsumerBean(consumerSegment);
	}

	public List<String[]> getAddressSegment() {
		return addressSegment_;
	}

	public void setAddressSegment(List<String[]> addressSegment) {
		addressSegment_ = addressSegment;

		addressList_ = BeanBuilderUtility.buildAddressBean(addressSegment);
	}

	public List<String[]> getAccountSegment() {
		return accountSegment_;
	}

	public void setAccountSegment(List<String[]> accountSegment) {
		accountSegment_ = accountSegment;
		
		accountList_ = BeanBuilderUtility.buildAccountBean(accountSegment);
	}

	public Consumer getConsumer() {
		return consumer_;
	}

	public void setConsumer(Consumer consumer) {
		consumer_ = consumer;
	}

	public List<Account> getAccount() {
		return accountList_;
	}

	public void setAccount(List<Account> account) {
		accountList_ = account;
	}

	public List<Address> getAddress() {
		return addressList_;
	}

	public void setAddress(List<Address> address) {
		addressList_ = address;
	}

	public Boolean getValid() {
		return valid_;
	}

	public void setValid(Boolean valid) {
		valid_ = valid;
	}

	public String getReasonForFailure() {
		return reasonForFailure_;
	}

	public void setReasonForFailure(String reasonForFailure) {
		reasonForFailure_ = reasonForFailure;
	}

	public Long getGlobalCrc() {
		return globalCrc_;
	}

	public void setGlobalCrc(Long globalCrc) {
		globalCrc_ = globalCrc;
		
		//ExperianCRC32.getInstance().getGlobalCRCLTI(address,consumer);
		calculateGlobalCRC();
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	public Boolean getHeaderTrailerIndicator() {
		return headerTrailerIndicator;
	}

	public void setHeaderTrailerIndicator(Boolean headerTrailerIndicator) {
		this.headerTrailerIndicator = headerTrailerIndicator;
	}
	
	/*public void SetReasonOfFailureForCounter(String reasonOfFailureForCounter) {
		reasonOfFailureForCounter_ = reasonOfFailureForCounter;
	}
	
	public String getReasonOfFailureForCounter {
		return reasonOfFailureForCounter_;
	}*/
	
	public String getReasonOfFailureForCounter() {
		return reasonOfFailureForCounter_;
	}

	public void setReasonOfFailureForCounter(String reasonOfFailureForCounter) {
		reasonOfFailureForCounter_ = reasonOfFailureForCounter;
	}

	@Override
	public String toString() {
		return "MfiDomain [mfiId_=" + mfiId_ + ", memberId_=" + memberId_ + ", rptDate_=" + rptDate_
				+ ", originalRecord_=" + originalRecord_ + ", consumerSegment_=" + Arrays.toString(consumerSegment_)
				+ ", addressSegment_=" + addressSegment_ + ", accountSegment_=" + accountSegment_ + ", consumer_="
				+ consumer_ + ", accountList_=" + accountList_ + ", addressList_=" + addressList_ + ", valid_=" + valid_
				+ ", reasonForFailure_=" + reasonForFailure_ + ", globalCrc_=" + globalCrc_
				+ ", headerTrailerIndicator=" + headerTrailerIndicator + "]";
	}

	

	public Long calculateGlobalCRC() {
		return ExperianCRC32.getInstance().getGlobalCRCLTI(addressList_,consumer_);
	}
}