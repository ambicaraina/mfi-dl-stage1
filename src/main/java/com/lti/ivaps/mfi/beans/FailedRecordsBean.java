package com.lti.ivaps.mfi.beans;

import java.util.List;

public class FailedRecordsBean {
	private String originalRecord_;
	private String reasonOfFailure;
	private String severity;
	
	public String getOriginalRecord() {
		return originalRecord_;
	}



	public void setOriginalRecord(String originalRecord) {
		originalRecord_ = originalRecord;
	}



	public String getReasonOfFailure() {
		return reasonOfFailure;
	}



	public void setReasonOfFailure(String reasonOfFailure) {
		this.reasonOfFailure = reasonOfFailure;
	}



	public String getSeverity() {
		return severity;
	}



	public void setSeverity(String severity) {
		this.severity = severity;
	}


	@Override
	public String toString() {
		return "FailedRecordsBean [originalRecord_=" + originalRecord_ + ", reasonOfFailure=" + reasonOfFailure
				+ ", severity=" + severity + "]";
	}



	public static FailedRecordsBean createFailedRecord(MfiDomain mfiDomain) {
		FailedRecordsBean failedRecordsBean = new FailedRecordsBean();

	
		/*if(!mfiDomain.getValid() && !mfiDomain.getHeaderTrailerIndicator()){
			failedRecordsBean.setOriginalRecord(mfiDomain.getOriginalRecord());
			failedRecordsBean.setReasonOfFailure(mfiDomain.getReasonForFailure());
		}*/
		if(!mfiDomain.getHeaderTrailerIndicator()){
			if(!mfiDomain.getValid()){
				failedRecordsBean.setOriginalRecord(mfiDomain.getOriginalRecord());
				failedRecordsBean.setReasonOfFailure(mfiDomain.getReasonForFailure());
				failedRecordsBean.setSeverity("ERROR");
			}else{
				if(!checkFailSegments(mfiDomain)){
					failedRecordsBean.setOriginalRecord(mfiDomain.getOriginalRecord());
					failedRecordsBean.setReasonOfFailure(mfiDomain.getReasonForFailure());
					failedRecordsBean.setSeverity("WARNING");
				}
			}
			
		}
		/*for (int i = 0; i < accountList.size(); i++) {
			if(!accountList.get(i).getAccountValid()){
				accountList.remove(i);
			}
		}*/
	
		return failedRecordsBean;
		
	}
	
	public static Boolean checkFailSegments(MfiDomain mfiDomain){
		Boolean indicator = true;
		List<Account> accountList = mfiDomain.getAccount();
		List<Address> addressList = mfiDomain.getAddress();
		Consumer consumer = mfiDomain.getConsumer();
		for (Address address : addressList) {
			if(!address.getAddressValid()){
				indicator= false;
			}
		}
		for (Account account : accountList) {
			if(!account.getAccountValid()){
				indicator= false;
			}			
		}
		return indicator;
		
	}
	
}
