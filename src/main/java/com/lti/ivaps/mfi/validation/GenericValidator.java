package com.lti.ivaps.mfi.validation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.lti.ivaps.mfi.beans.MfiDomain;
import com.lti.ivaps.mfi.error.ErrorCodeList;
import com.lti.ivaps.mfi.exceptions.CdfValidationException;

public class GenericValidator {
	
	Boolean validationIndicator = false;
	/*public Boolean validateMe(String fieldValue, Boolean stringCheck, int minValue, int maxValue){
		
		
		
		if(stringCheck){
			if(StringUtils.isNotBlank(fieldValue)  && fieldValue.length() >= minValue && fieldValue.length() <= maxValue){
				validationIndicator = true;
			}
		}else{
			try {
				Integer.parseInt(fieldValue);
				if(fieldValue.matches("d{maxValue}")){
					validationIndicator = true;
				}
			} catch (NumberFormatException e) {
				System.out.println("Field Value not in correct format");				
			}
			
			
		}
		return validationIndicator;
	}*/
	
	public static Boolean validateMe(String fieldValue, Boolean blankCheck, Boolean integerCheck, Boolean numericCheck, Boolean dateCheck, Integer minLength, Integer maxLength){
		
		if (blankCheck && StringUtils.isBlank(fieldValue)) {
			return false;
		}
		
		if (integerCheck && StringUtils.isNotBlank(fieldValue)) {
			try {
				Integer.parseInt(fieldValue);
			} catch (NumberFormatException e) {
				return false;
			}
		}
		
		if (numericCheck && StringUtils.isNotBlank(fieldValue)) {
			try {
				Double.parseDouble(fieldValue);
			} catch (NumberFormatException e) {
				return false;
			}
		}
		
		// date check here
		
		if (null != minLength && minLength > 0 && StringUtils.length(fieldValue) < minLength) {
			return false;
		}
		
		if (null != maxLength && StringUtils.length(fieldValue) > maxLength) {
			return false;
		}
		
		if(dateCheck && StringUtils.isNotBlank(fieldValue)){
			String[] dateFormats = {"ddMMyyyy", "dMMyyyy"};
			for (String dateFormat : dateFormats) {
			SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
			sdf.setLenient(false);
			try {
				sdf.parse(fieldValue.trim());
				break;
			} catch (ParseException e) {
				
				return false;
			}
		}
			return true;
		}
		
		
		return true;
	}
		
	
}
