package com.lti.ivaps.mfi.exceptions;

public class CdfParsingException extends Exception{
	
	public CdfParsingException(String e){
		super(e);
		System.out.println("File not parsed properly!!!!");
	}
}
