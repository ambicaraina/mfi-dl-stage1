package com.lti.ivaps.mfi.dataframes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.lti.ivaps.mfi.error.ErrorCodeList;

public class SummaryStatistics implements Serializable{
	
	private String Summary;
	private int Count;
	
	
	

	public SummaryStatistics(String summary, int count) {
		super();
		Summary = summary;
		Count = count;
	}

	public static List<SummaryStatistics> generateErrorList(int totalCount, int passedCount, int failedCount,
			int countOfE001, int countOfE002, int countOfE003, int countOfE004, int countOfE005, int countOfE006, int countOfE007,
			int countOfEACT002, int countOfEACT003, int countOfEACT004, int countOfEACT005,
			int countOfEACT006, int countOfEACT007, int countOfEACT008, int countOfEACT011,
			int countOfEACT012, int countOfEACT015, int countOfEACT019, int countOfEACT020,
			int countOfEACT023, int countOfEACT024, int countOfEACT025, int countOfEADR000,
			int countOfECNS002, int countOfECNS003, int countOfECNS004, int countOfECNS006, int countOfECNS010,
			int countOfECNS011, int countOfECNS012, int countOfECNS013, int countOfECNS015_024) {
		
		List<SummaryStatistics> summaryStats = new ArrayList();

		summaryStats.add(new SummaryStatistics("Total Records", totalCount));
		summaryStats.add(new SummaryStatistics("Passed Records", passedCount));
		summaryStats.add(new SummaryStatistics("Failed Records", failedCount));
		summaryStats.add(new SummaryStatistics("Invalid record ("+ErrorCodeList.E001+")", countOfE001));
		summaryStats.add(new SummaryStatistics("No account Record found ("+ErrorCodeList.E002+")", countOfE002));
		summaryStats.add(new SummaryStatistics("No address segment found	 ("+ErrorCodeList.E003	+")" ,countOfE003));
		summaryStats.add(new SummaryStatistics("More than one customer record found	 ("+ErrorCodeList.E004	+")" ,countOfE004));
		summaryStats.add(new SummaryStatistics("Record not in proper format	 ("+ErrorCodeList.E005	+")" ,countOfE005));
		summaryStats.add(new SummaryStatistics("Invalid record in segment	 ("+ErrorCodeList.E006	+")" ,countOfE006));
		summaryStats.add(new SummaryStatistics("Field Value is not a number	 ("+ErrorCodeList.E007	+")" ,countOfE007));
		summaryStats.add(new SummaryStatistics("Invalid Member Identifier	 ("+ErrorCodeList.EACT002	+")" ,countOfEACT002));
		summaryStats.add(new SummaryStatistics("Invalid Branch Identifier	 ("+ErrorCodeList.EACT003	+")" ,countOfEACT003));
		summaryStats.add(new SummaryStatistics("Invalid Center Identifier	 ("+ErrorCodeList.EACT004	+")" ,countOfEACT004));
		summaryStats.add(new SummaryStatistics("Invalid Loan officer for originating the loan	 ("+ErrorCodeList.EACT006	+")" ,countOfEACT006));
		summaryStats.add(new SummaryStatistics("Invalid Date of account information	 ("+ErrorCodeList.EACT007	+")" ,countOfEACT007));
		summaryStats.add(new SummaryStatistics("Invalid Loan category	 ("+ErrorCodeList.EACT008	+")" ,countOfEACT008));
		summaryStats.add(new SummaryStatistics("Invalid Loan purpose	 ("+ErrorCodeList.EACT011	+")" ,countOfEACT011));
		summaryStats.add(new SummaryStatistics("Invalid Account Status	 ("+ErrorCodeList.EACT012	+")" ,countOfEACT012));
		summaryStats.add(new SummaryStatistics("Invalid Account opened date	 ("+ErrorCodeList.EACT015	+")" ,countOfEACT015));
		summaryStats.add(new SummaryStatistics("Invalid sanctioned load amount	 ("+ErrorCodeList.EACT019	+")" ,countOfEACT019));
		summaryStats.add(new SummaryStatistics("Invalid Total disbursed amount	 ("+ErrorCodeList.EACT020	+")" ,countOfEACT020));
		summaryStats.add(new SummaryStatistics("Invalid Installment amount	 ("+ErrorCodeList.EACT023	+")" ,countOfEACT023));
		summaryStats.add(new SummaryStatistics("Invalid current balance	 ("+ErrorCodeList.EACT024	+")" ,countOfEACT024));
		summaryStats.add(new SummaryStatistics("Invalid amount overdue	 ("+ErrorCodeList.EACT025	+")" ,countOfEACT025));
		summaryStats.add(new SummaryStatistics("Invalid address segment	 ("+ErrorCodeList.EADR000	+")" ,countOfEADR000));
		summaryStats.add(new SummaryStatistics("Invalid member identifier	 ("+ErrorCodeList.ECNS002	+")" ,countOfECNS002));
		summaryStats.add(new SummaryStatistics("Invalid branch identifier	 ("+ErrorCodeList.ECNS003	+")" ,countOfECNS003));
		summaryStats.add(new SummaryStatistics("Invalid center identifier	 ("+ErrorCodeList.ECNS004	+")" ,countOfECNS004));
		summaryStats.add(new SummaryStatistics("Invalid member first name	 ("+ErrorCodeList.ECNS006	+")" ,countOfECNS006));
		summaryStats.add(new SummaryStatistics("Invalid member first name	 ("+ErrorCodeList.ECNS010	+")" ,countOfECNS010));
		summaryStats.add(new SummaryStatistics("Invalid member age	 ("+ErrorCodeList.ECNS011	+")" ,countOfECNS011));
		summaryStats.add(new SummaryStatistics("Invalid member age as on date	 ("+ErrorCodeList.ECNS012	+")" ,countOfECNS012));
		summaryStats.add(new SummaryStatistics("Invalid gender of member	 ("+ErrorCodeList.ECNS013	+")" ,countOfECNS013));
		summaryStats.add(new SummaryStatistics("Invalid key person or relative information	 ("+ErrorCodeList.ECNS015_024	+")" ,countOfECNS015_024));
		//System.out.println(summaryStats);
		return summaryStats;
	}

	public String getSummary() {
		return Summary;
	}

	public void setSummary(String summary) {
		Summary = summary;
	}

	public int getCount() {
		return Count;
	}

	public void setCount(int count) {
		Count = count;
	}

	@Override
	public String toString() {
		return "SummaryStatistics [Summary=" + Summary + ", Count=" + Count + "]";
	}

}
