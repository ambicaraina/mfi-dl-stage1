package com.lti.ivaps.mfi;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.hive.HiveContext;

import com.lti.ivaps.mfi.beans.Account;
import com.lti.ivaps.mfi.beans.MfiDomain;

public class DataFramesFactory {

	public static DataFrame createDataFrame(JavaRDD<?> rdd, Class<?> cls, HiveContext hiveContext) {
		DataFrame createDataFrame = hiveContext.createDataFrame(rdd, cls);
		return createDataFrame;
		
	}
	
	public static DataFrame createAccountDataFrame(JavaRDD<Account> map, Class<Account> account, HiveContext hiveContext) {
		DataFrame createDataFrame = hiveContext.createDataFrame(map, account);
		return createDataFrame;
		
	}
	
	public static DataFrame createDataFrameNew(JavaRDD<MyClass> map, Class<MyClass> class1, HiveContext hiveContext) {
		DataFrame createDataFrame = hiveContext.createDataFrame(map, class1);
		return createDataFrame;
		
	}
	
	//For testing purpose
	public static DataFrame createDataFrameSql(JavaRDD<?> rdd, Class<?> cls, SQLContext sqlContext) {
		DataFrame createDataFrame = sqlContext.createDataFrame(rdd, cls);
		
		return createDataFrame;
		
	}

}
