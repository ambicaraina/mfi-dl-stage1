package com.lti.ivaps.mfi.test;

public class Data {
	private String singleRecord;

	public String getSingleRecord() {
		return singleRecord;
	}

	public void setSingleRecord(String singleRecord) {
		this.singleRecord = singleRecord;
	}

	@Override
	public String toString() {
		return "Data [singleRecord=" + singleRecord + "]";
	}
	
	
}
